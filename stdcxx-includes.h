
#ifndef __STDCXX_INCLUDES_H__
#define __STDCXX_INCLUDES_H__

#include "stdc-includes.h"

#ifdef HAVE_IOSTREAM
# include <iostream>
#else
# ifdef HAVE_IOSTREAM_H
#   include <iostream.h>
# else
#   error No support for C++ iostream.
# endif
#endif

#ifdef HAVE_FSTREAM
# include <fstream>
#else
# ifdef HAVE_FSTREAM_H
#   include <fstream.h>
# else
#   error No support for C++ fstream.
# endif
#endif

#ifdef HAVE_IOMANIP
# include <iomanip>
#else
# ifdef HAVE_IOMANIP_H
#   include <iomanip.h>
# else
#   error No support for C++ iomanip.
# endif
#endif

#endif /* __STDCXX_INCLUDES_H__ */

