
#ifndef __STDC_INCLUDES_H__
#define __STDC_INCLUDES_H__

#include <config.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#   include <stdlib.h>
# endif
#endif
#ifdef HAVE_ALLOCA_H
# include <alloca.h> 
#elif defined __GNUC__ 
# define alloca __builtin_alloca 
#elif defined _AIX 
# define alloca __alloca 
#elif defined _MSC_VER 
# include <malloc.h> 
# define alloca _alloca 
#else 
# ifndef HAVE_ALLOCA 
#   ifdef __cplusplus
extern "C" 
#   endif
void *alloca (size_t); 
# endif 
#endif

#ifdef HAVE_STDIO_H
# include <stdio.h>
#else
# error No support for stdio.h.
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# error No support for stdlib.h.
#endif

#ifdef HAVE_STDARG_H
# include <stdarg.h>
#else
# error No support for stdarg.h.
#endif

#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#else
# ifdef HAVE_STDINT_H
#   include <stdint.h>
# else
#   error No support for integer types.
# endif
#endif

#ifdef HAVE_CTYPE_H
# include <ctype.h>
#else
# error No support for ctype.h.
#endif

#ifdef HAVE_MATH_H
# include <math.h>
#else
# error No support for math.h.
#endif

#ifdef HAVE_FLOAT_H
# include <float.h>
#else
# error No support for float.h.
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# error No support for string.h.
#endif

#endif /* __STDC_INCLUDES_H__ */

